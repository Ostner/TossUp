//
//  State.swift
//  TossUp
//
//  Created by Tobias Ostner on 25.09.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

struct State {
    
    static let shared = State(lower: 0, upper: 10)
    
    var lower: UInt32
    var upper: UInt32
    
}
