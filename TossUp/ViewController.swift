//
//  ViewController.swift
//  TossUp
//
//  Created by Tobias Ostner on 24.09.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var result: UIButton!
    
    @IBAction func toss() {
        let toss = arc4random_uniform(State.shared.upper) + 1
        result.setTitle("\(toss)", for: .normal)
    }

}

