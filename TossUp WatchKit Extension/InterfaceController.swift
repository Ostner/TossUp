//
//  InterfaceController.swift
//  TossUp WatchKit Extension
//
//  Created by Tobias Ostner on 24.09.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var result: WKInterfaceButton!
    
    @IBAction func toss() {
        let toss = arc4random_uniform(State.shared.upper) + 1
        result.setTitle("\(toss)")
    }

}
